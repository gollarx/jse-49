package ru.t1.shipilov.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
